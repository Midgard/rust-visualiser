extern crate ndarray;
use ndarray::{Array1, Array2, linalg};

use std::f32::consts::PI;

struct Wave {
	factor: f32
}
impl Wave {
	fn new(freq: u32, sample_rate: u32) -> Self {
		Wave{ factor: 2.0 * PI * freq as f32 / sample_rate as f32 }
	}

	fn at(&self, t: u32) -> f32 {
		t as f32 * self.factor
	}
	pub fn sin_at(&self, t: u32) -> f32 {
		self.at(t).sin()
	}
	pub fn cos_at(&self, t: u32) -> f32 {
		self.at(t).cos()
	}
}

// Created in this function
//       ↓
// [[sin  sin  …  ],    [s1,     [x1,
//  [cos  cos  …  ],  *  s2,  =   y1,
//  [sin  sin  …  ],     s3,      x2,
//  [cos  cos  …  ]…]    s4…]     y2…]
pub fn matrix(
	inputs: usize,
	frequencies: &Vec<u32>,
	sample_rate: u32
) -> Array2<f32> {

	let mut matrix = Array2::<f32>::zeros((frequencies.len() * 2, inputs));

	for (i, freq) in frequencies.iter().enumerate() {
		let wave = Wave::new(*freq as u32, sample_rate);

		for j in 0..inputs {
			matrix[[2 * i,     j]] = wave.sin_at(j as u32);
			matrix[[2 * i + 1, j]] = wave.cos_at(j as u32);
		}
	}

	matrix
}

pub fn transform(
	signal: &Array1<f32>,
	fourier_matrix: &Array2<f32>
) -> Array1<f32> {

	let mut fourier_coords = Array1::<f32>::zeros(fourier_matrix.nrows());

	linalg::general_mat_vec_mul(
		1.0, &fourier_matrix, &signal,
		0.0, &mut fourier_coords
	);

	let n_fourier_coeffs = fourier_coords.len() / 2;
	let mut fourier_coeffs = Array1::<f32>::zeros(n_fourier_coeffs);
	for i in 0..n_fourier_coeffs {
		fourier_coeffs[i] = (fourier_coords[2 * i].powi(2) + fourier_coords[2 * i + 1].powi(2)).sqrt()
	}

	fourier_coeffs
}

#[allow(dead_code)]
pub fn debug_print_fourier(fourier: &Array2<f32>) {
	println!("[");
	for row in fourier.genrows() {
		println!(
			"{:?},",
			row.to_vec()
		);
	}
	println!("]");
}
